package com.example.marvelmovies

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SimpleAdapter
import com.caverock.androidsvg.SVG
import com.example.marvelmovies.Models.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_about.*

object globalComics {
    val list = ArrayList<ComicsItem>()
}

fun openUrl(url: String, activity: FragmentActivity?) {
    val builder = CustomTabsIntent.Builder()
    builder.enableUrlBarHiding()
    builder.setShowTitle(true)
    builder.setToolbarColor(Color.TRANSPARENT)
    val customTabsIntent = builder.build()
    customTabsIntent.launchUrl(activity, Uri.parse(url))
}

class ComicsFragment : Fragment() {
    private lateinit var listviewComics: ListView
    private var comicsList = ArrayList<ComicsItem>()
    private lateinit var adapterComicsList: AdapterComicsList

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.fragment_comics, container, false
    )

    private fun finishInit() {
        listviewComics = view!!.findViewById(R.id.listview_comics)
        adapterComicsList = AdapterComicsList(this.context!!, comicsList)
        listviewComics.adapter = adapterComicsList
    }

    @SuppressLint("CheckResult")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (globalComics.list.isEmpty())
            NetHelper.instance.getComics()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                    onNext = {
                        Log.d("MainActivity", it.toString())
                        for (i in 0 until it.data.results.count()) {
                            val desc =
                                if (it.data.results[i].description != null) it.data.results[i].description else "Описание отсутствует"
                            val comic = ComicsItem(
                                it.data.results[i].id,
                                it.data.results[i].title,
                                it.data.results[i].prices[0].price,
                                "${it.data.results[i].thumbnail.path}/portrait_xlarge.${it.data.results[i].thumbnail.extension}",
                                desc
                            )
                            comicsList.add(comic)
                            globalComics.list.add(comic)
                            Log.d("MainActivity", it.data.results[i].title + " was added")

                            finishInit()
                        }
                    },
                    onError = {
                        Log.d("MainActivity", it.toString())
                    }
                )
        else {
            comicsList = globalComics.list
            finishInit()
        }
    }

}

class CreatorsFragment : Fragment() {
    private lateinit var listviewCreators: ListView
    private lateinit var adapterCreators: AdapterCreators


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.fragment_creators, container, false
    )
    @SuppressLint("CheckResult")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val list = ArrayList<CreatorItem>()
        list.add(CreatorItem(137733492, "Александр", "Каштанов", "https://sun9-44.userapi.com/c841428/v841428940/297d7/yhYPSnTb3oE.jpg?ava=1", "alex_wite"))
        list.add(CreatorItem(133358386, "Данила", "Родичкин", "https://sun9-46.userapi.com/c857732/v857732390/138b7e/83iHvW4S1GQ.jpg?ava=1", "daniluk4000"))
        list.add(CreatorItem(34390353, "Виктория", "Кузьменко", "https://sun1-22.userapi.com/c855236/v855236349/6941b/-1umclLGIlA.jpg?ava=1", "yellowsonate"))
        list.add(CreatorItem(95310371, "Дмитрий", "Гыждиян", "https://sun1-29.userapi.com/c846017/v846017016/c63e8/VNv2S0Asfzo.jpg?ava=1", "dimagyzhdian"))
        list.add(CreatorItem(59163864, "Настя", "Курмаз", "https://sun9-56.userapi.com/c853424/v853424774/12da2/Qg1iYZfn0qc.jpg?ava=1", "anastasia_kurmaz"))
        list.add(CreatorItem(153284283, "Дарья", "Барашкова", "https://sun1-15.userapi.com/c850124/v850124971/17bf65/4KSMuvyquoE.jpg?ava=1", "DariaBarashkova"))

        listviewCreators = view!!.findViewById(R.id.listview_creators)
        adapterCreators = AdapterCreators(this.context!!, list, activity)
        listviewCreators.adapter = adapterCreators
    }
}

class FavouritesFragment : Fragment() {
    private lateinit var listviewComics: ListView
    private lateinit var adapterComicsList: AdapterComicsList

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.fragment_favorites, container, false
    )

    @SuppressLint("CheckResult")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val db = DB(context, null)

        listviewComics = view!!.findViewById(R.id.listview_favorites)
        adapterComicsList = AdapterComicsList(this.context!!, db.getFavourites())
        listviewComics.adapter = adapterComicsList
    }
}

class AboutFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.fragment_about, container, false
    )

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        description.text =
            "${getString(R.string.description_title)} \n\n${getString(R.string.description_body)}"

        val svg = SVG.getFromResource(resources, R.raw.logo)

        val bitmap = Bitmap.createBitmap(312, 239, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        svg.renderToCanvas(canvas)

        logo.background = BitmapDrawable(resources, bitmap)

        link.setOnClickListener {
            val url = getString(R.string.url)
            openUrl(url, activity)
        }

        gitlab.setOnClickListener {
            val url = getString(R.string.gitlab)
            openUrl(url, activity)
        }


    }
}