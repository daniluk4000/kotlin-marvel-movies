package com.example.marvelmovies.Models

class CreatorItem(
    var id: Int,
    var first_name: String,
    var last_name: String,
    var avatar: String,
    var gitlab: String
) {
}