package com.example.marvelmovies.Models

class ComicsItem(
    var id: Int,
    var title: String,
    var price: Double,
    var thumbnail: String,
    var description: String
) {
}