package com.example.marvelmovies.Models

import android.content.Context
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.marvelmovies.R
import com.example.marvelmovies.openUrl
import java.util.ArrayList

class AdapterCreators(private val context: Context, creatorsItems: ArrayList<CreatorItem>, activity: FragmentActivity?) :
    BaseAdapter() {
    private var activity:FragmentActivity? = activity
    private var creatorsItems: List<CreatorItem> = creatorsItems
    private var inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return creatorsItems.size
    }

    override fun getItem(i: Int): Any {
        return creatorsItems[i]
    }

    override fun getItemId(i: Int): Long {
        return creatorsItems[i].id.toLong()
    }

    private fun getCreatorsItem(position: Int): CreatorItem {
        return getItem(position) as CreatorItem
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        var localeView: View? = view

        if (localeView == null) {
            localeView = inflater.inflate(R.layout.listview_item_creators, viewGroup, false)
        }

        val item = getCreatorsItem(i)

        Glide.with(localeView)
            .load(item.avatar)
            .into(localeView!!.findViewById(R.id.imageView))

        val title = localeView.findViewById(R.id.textView) as TextView
        title.text = "${item.first_name} ${item.last_name}"

        var gitlabButton = localeView.findViewById(R.id.gitlabProfile) as Button
        var vkButton = localeView.findViewById(R.id.vkProfile) as Button

        gitlabButton.setOnClickListener {
            openUrl("https://gitlab.com/${item.gitlab}", activity)
        }
        vkButton.setOnClickListener {
            openUrl("https://vk.com/id${item.id}", activity)
        }

        return localeView
    }
}
