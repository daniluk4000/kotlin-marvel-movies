package com.example.marvelmovies.Models

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.marvelmovies.NetHelper
import com.example.marvelmovies.globalComics
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Favorites(
    @SerializedName("_id") var id: Number = 0,
    @SerializedName("comics_item_id") var comicsItemId: Number = 0
) : Serializable

class DB(context: Context?, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "com.example.marvelmovies.db"
        private const val DATABASE_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
            "CREATE TABLE favourites (" +
                    "_id INTEGER PRIMARY KEY," +
                    "comics_item_id INTEGER" +
                    ")"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS favourites");
        onCreate(db)
    }

    fun addToFavourites(item: ComicsItem): Int {
        val values = ContentValues()
        values.put("comics_item_id", item.id)

        val db = this.writableDatabase

        if (isFavourite(item)) return -1

        val id = db.insert("favourites", null, values)

        Log.d("DB", id.toString())
        db.close()

        return id.toInt()
    }

    fun removeFromFavourites(item: ComicsItem) {
        if(!isFavourite(item)) return

        val db = this.writableDatabase;

        db.delete("favourites", "comics_item_id = ?", arrayOf(item.id.toString()))

        db.close()
    }

    fun isFavourite(item: ComicsItem): Boolean {
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM favourites WHERE comics_item_id = ${item.id}", null)

        if (!cursor.moveToFirst()) return false;

        val isFav = cursor.getInt(1) == item.id

        cursor.close()
        db.close()

        return isFav
    }

    fun getFavourites(): ArrayList<ComicsItem> {
        val list = ArrayList<ComicsItem>()
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM favourites", null)
        val comics = globalComics.list


        if (cursor.moveToFirst()) {
            cursor.moveToFirst()

            do {
                val filteredList = comics.filter { x -> x.id == cursor.getInt(1) }
                if (filteredList.isEmpty()) continue
                val comic = filteredList[0]

                list.add(comic)
            } while (cursor.moveToNext())
        }

        return list
    }
}