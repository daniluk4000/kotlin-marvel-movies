package com.example.marvelmovies

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import kotlinx.android.synthetic.main.activity_comics.*

class ComicsActivity : AppCompatActivity() {

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comics)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.app_name)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onResume() {
        super.onResume()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val fragment = ComicsFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmenti, fragment, fragment.javaClass.getSimpleName())
            .commit()

        navigation.menu.findItem(R.id.navigation_about).isChecked = false
        navigation.menu.findItem(R.id.navigation_favourite).isChecked = false
        navigation.menu.findItem(R.id.navigation_creators).isChecked = false
        navigation.menu.findItem(R.id.navigation_comics).isChecked = true

        toolbar.title =getString(R.string.title_comics)
    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_comics -> {
                val fragment = ComicsFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmenti, fragment, fragment.javaClass.getSimpleName())
                    .commit()
                toolbar.title = getString(R.string.title_comics)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_creators -> {
                val fragment = CreatorsFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmenti, fragment, fragment.javaClass.getSimpleName())
                    .commit()
                toolbar.title = getString(R.string.title_creators)

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favourite -> {
                val fragment = FavouritesFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmenti, fragment, fragment.javaClass.getSimpleName())
                    .commit()
                toolbar.title = getString(R.string.title_favourite)

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_about -> {
                val fragment = AboutFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmenti, fragment, fragment.javaClass.getSimpleName())
                    .commit()
                toolbar.title = getString(R.string.title_about)

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
