# КиноЛента

## Информация по сборке 

[![pipeline status](https://gitlab.com/daniluk4000/kotlin-marvel-movies/badges/master/pipeline.svg)](https://gitlab.com/daniluk4000/kotlin-marvel-movies/commits/master) 
[Скачать apk](https://gitlab.com/daniluk4000/kotlin-marvel-movies/-/jobs/artifacts/master/raw/app/build/outputs/apk/debug/app-debug.apk?inline=false&job=assembleDebug) - при условии, что pipeline: `passed`
 
[Trello проекта](https://trello.com/b/McmQRVhs/%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F)

## Авторизация
* Введите пароль админ
* Нажмите `Авторизация`
* Ваш логин будет сохранён

## Управление "избранным"
* Чтобы добавить в избранное, нажмите на иконку звездочки в общем списке комиксов (1-я вкладка)
* Иконка станет закрашенной
* Чтобы удалить из избранного, повторно нажмите на иконку звездочки

## Раздел "избранное"
* Чтобы удалить из избранного, нажмите на закрашенную иконку звездочки
* Иконка станет не закрашенной, но комикс пропадет из списка только после перезахода на вкладку/приложение
* Если вы нажали на кнопку случайно, не покидая вкладку/приложение повторно нажмите на иконку звездочки